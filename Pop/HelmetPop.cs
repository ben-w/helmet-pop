﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ModLoader.Framework;
using ModLoader.Framework.Attributes;
using SteamQueries.Models;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Pop
{
    [ItemId("marsh-helmet-pop")]
    public class HelmetPop : VtolMod
    {
        private AudioClip _clip;
        
        private void Awake()
        {
            var directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            StartCoroutine(LoadAudioClip(directory));
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private IEnumerator LoadAudioClip(string directory)
        {
            var path = Path.Combine(directory, "pop.wav");
            if (!File.Exists(path))
            {
                LogError($"Couldn't find the pop file at '{path}'");
                yield break;
            }
            
            Log($"Loading pop file from {path}");
            using (var www = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.WAV))
            {
                yield return www.SendWebRequest();
                _clip = DownloadHandlerAudioClip.GetContent(www);
            }
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            if (!scene.name.Equals("LoadingScene"))
            {
                return;
            }

            var sceneParent = GameObject.Find("SceneParent");

            if (sceneParent == null)
            {
                LogError($"{nameof(sceneParent)} is null, can't apply pop sound");
                return;
            }

            var seat = sceneParent.transform.Find("seat");

            if (seat == null)
            {
                LogError($"{nameof(seat)} is null, can't apply pop sound");
                return;
            }

            var audioSource = seat.GetComponent<AudioSource>();
            if (audioSource == null)
            {
                LogError($"The gameobject {seat.gameObject.name} does not have a type {nameof(AudioSource)} attached");
                return;
            }
            
            audioSource.clip = _clip;
            Log("Set the audio clip!");
        }

        public override void UnLoad()
        {
            Log("Bye!");
        }

        private static void LogError(object message) => Debug.LogError($"[{nameof(HelmetPop)}]{message}");
        private static void Log(object message) => Debug.Log($"[{nameof(HelmetPop)}]{message}");
    }
}